# Gwiezdny Patrol 

Prosta gra 2D inpirowana uniwersum Kapitana Bomby, napisana w Python (2.7) oraz przy użyciu PyGame. Projekt zaliczeniowy na studia. 

Jako, że najistotniejsza była nauka, starałem się wykorzystywać jak najmniej wbudowanej funkcjonalności PyGame - system kolizji obiektów oraz (bardzo prosty) zachowywania się przeciwników, stworzyłem samemu. 

Niestety jest to wersja dość wczesna (beta) - finalna wersja została utracona (były m.in. dźwięki, różni kosmici oraz system rankingowy zapisywany na zewnętrznym serwerze wraz z stroną www przedstawiającą wyniki).  

## Video 
[![Watch the video](readme_files/GwiezdnyPatrol_img.png)](readme_files/GwiezdnyPatrol.mp4)
