#include bibliotek
import pygame
from math import *
from sys import *
from os import *
from random import *
from time import *
from pygame.locals import *

class Patrol(object):
    def __init__(self):
        # ruszamy z kopyta -> pygame
        pygame.init()

        # konfig ekranu
        self.glowne = pygame.display.set_mode((400,430),DOUBLEBUF)
        # ustawienie tytulu okna
        pygame.display.set_caption("Gwiezdny Patrol")

        # ogolny konfig
        # stan gry: 1 - start; 0 - wyjscie
        self.gamestate = 1
        self.orzel_szybkosc = 0.5 # szybkosc orzela
        self.orzel_x = 0   # pozycja X orzela
        self.orzel_y = 180   # startowa pozycja Y orzela
        self.orzel_animacja = 1
        self.orzel_life = 3
        self.kolizja = 0
        self.orzel_y_old = 0
        #zmienne do liczenia trwania czasu w lvl
        self.lvl_start = 0
        self.lvl_end = 0
        self.time_pauza = 0
        #czity
        self.godmode = 0


        self.petla = 0


        #okresla co sie dzieje obecnie w petli
        self.mode = 0
        #0 - menu startowe
        #1 - lvl oparty na unikaniu meteorytow
        #2 - lvl walka z kosmita
        #3 - koniec gry - napisy koncowe

        #ustalanie wartosci modyfikowalnych dla meteorow
        self.met_max = 10; #max meteorow na planszy
        self.met_szybkosc = 0.1 #szybkosc poruszania sie meteorow
        self.met_time_ile = 1
        self.met_time = 0 # czas kiedy ostatni meteoryt wygenerowano
        self.met_tablica = [] #tablica z wspolrzednymi meteorytow

        #po utracie zycia
        self.orzel_zdechl_inicjacja = 0
        self.orzel_zdechl_start = 0
        self.orzel_zdechl_end = 0
        self.orzel_zdechl_napis_start = 0
        self.orzel_zdechl_napis_end = 0

        #wczytywanie obrazkow do pamieci podrecznej gry
        # wczytytanie tla
        self.tlo = pygame.image.load("img/kosmos.jpg").convert()
        self.tlo_pauza = pygame.image.load("img/pauza.jpg").convert()
        #menu startowe
        self.tlo_menu = pygame.image.load("img/start.jpg").convert()
        self.menu_belka = pygame.image.load("img/start_belka.jpg").convert()
        self.menu_font = pygame.font.SysFont("verdana",20)
        #gejm over
        self.tlo_gameover = pygame.image.load("img/gameover.jpg").convert()
        #tlo dla przerwy miedzy meteorami a walka z kosmita
        self.tlo_misja = pygame.image.load("img/misja.jpg").convert()
        # wczytanie obrazka meteorytu
        self.met = pygame.image.load("img/met_1.png").convert_alpha()
        # wczytywanie orzela
        self.orzel = pygame.image.load("img/orzel/orzel.png").convert_alpha()
        # wczytywanie obrazkow do animacji strumienia rakietowego orzela
        self.orzel_rakieta = []
        self.orzel_rakieta.append(pygame.image.load("img/orzel/rakieta/ogien_1.png").convert_alpha())
        self.orzel_rakieta.append(pygame.image.load("img/orzel/rakieta/ogien_1.png").convert_alpha())
        self.orzel_rakieta.append(pygame.image.load("img/orzel/rakieta/ogien_2.png").convert_alpha())
        self.orzel_rakieta.append(pygame.image.load("img/orzel/rakieta/ogien_3.png").convert_alpha())
        self.orzel_rakieta.append(pygame.image.load("img/orzel/rakieta/ogien_4.png").convert_alpha())
        self.orzel_rakieta.append(pygame.image.load("img/orzel/rakieta/ogien_5.png").convert_alpha())
        self.orzel_rakieta.append(pygame.image.load("img/orzel/rakieta/ogien_6.png").convert_alpha())\
        #pociski
        self.pocisk_orzel = pygame.image.load("img/pocisk_orzel.jpg").convert()
        self.pocisk_kosmita = pygame.image.load("img/pocisk_kosmita.jpg").convert()
        self.pocisk_time_ostatni = 0
        self.pociski_time = 0.3
        self.pociski_orzel = []
        self.pociski_kosmita = []
        self.pociski_szybkosc = 0.2
        #wczytywanie obrazkow do GUI dolnego
        self.pasek_gui = pygame.image.load("img/pasek_gui.jpg").convert()
        self.orzel_gui = pygame.image.load("img/orzel_smini.png").convert_alpha()
        #wczytywanie kosmitow
        self.kosmita = []
        self.kosmita.append(pygame.image.load("img/kosmity/kurvinox.png").convert_alpha())
        #konfig tymczasowy dla kosmity - pozniej bedzie tutaj if w zaleznosci od kosmity
        self.kosmita_ktory = 0 #kurvinox
        self.kosmita_life = 10
        self.kosmita_life_now = 10
        self.kosmita_speed = 0.25
        self.kosmita_x = 371
        self.kosmita_y = 180
        self.kosmita_pociski_time_ostatni = 0
        self.kosmita_pociski_time = 0.5

        # glowna petla - wywolanie, uruchomienie gry
        self.loop()
    # konczenie programu
    def game_exit(self):
        # koniec pracy na dzisiaj
        exit()

    def rusz_orzela(self,y):
        #modyfikacja pozycji orzela
        self.orzel_y = self.orzel_y + (y * self.orzel_szybkosc)
        #przemieszczanie orzela, kiedy poza plansza gry jest
        if self.orzel_y>360:
            self.orzel_y = 0;
        if self.orzel_y<0:
            self.orzel_y = 360;

    def load_orzel(self):

        #umieszczanie smugi silnika na planszy
        self.glowne.blit(self.orzel_rakieta[self.orzel_animacja],(self.orzel_x,self.orzel_y+20))
        #umieszczanie orzela na planszy
        self.glowne.blit(self.orzel,(self.orzel_x,self.orzel_y))

        #animacja orzela
        if self.orzel_animacja==6:
            self.orzel_animacja=1;
        else:
            self.orzel_animacja = self.orzel_animacja+1
        # END animacja orzela

        #return self.orzel

    def generate_met(self):
        #generowanie meteorytow
        if (time()>self.met_time) and (len(self.met_tablica)<self.met_max):
            #jezeli czas od utworzenia ostatniego meteorytu jest mniejszy
            # i jezeli w tablicy jeszcze nie ma max mozliwych meteorow
            # GENERUJEMY METEORYT

            #losowa pozycja nowego meteorytu na Y
            met_y = randint(0, 382)
            self.met_tablica.append([400,met_y])
            self.met_time=time()+self.met_time_ile

        for id in range(len(self.met_tablica)):
            # przesowanie meteorytow
            # umieszczanie meteorytow na planszy
            # enetualnie usuwanie kiedy sa po za plansza

            # poruszanie meteoryta
            zmiana = self.met_tablica[id][0];
            zmiana = zmiana + (-1 * self.met_szybkosc)
            self.met_tablica[id][0] = zmiana
            del(zmiana)
            # dodawanie go do renderowania
            self.glowne.blit(self.met,(self.met_tablica[id][0],self.met_tablica[id][1]))

    def del_met(self):
        #tworzenie tymczasowej tablicy z rekordami, ktore maja byc usuniete
        temp_tablica = []
        for id in range(len(self.met_tablica)):
            if -30>=self.met_tablica[id][0]:
                temp_tablica.append(id)

        for id in range(len(temp_tablica)):
            del(self.met_tablica[temp_tablica[id]])

    def spr_kolizje(self,x1,y1,w1,h1,x2,y2,w2,h2):
        if x1 >= x2+w2: return True
        if x1+w1 <= x2: return True
        if y1 >= y2+h2: return True
        if y1+h1 <= y2: return True
        return False

    def kolizje(self):
        #zmienna kolizja zmienia wartosc kiedy nastapi kolizja

        #self.orzel_x - pozycja x orzela
        #self.orzel_y - pozucja y orzela
        #wysokosc orzela 40
        #szerokosc orzela 100

        for id in range(len(self.met_tablica)):
            #self.met_tablica[id][0] - pozycja x meteoryta
            #self.met_tablica[id][1] - pozycja y meteoryta
            #wysokosc meteoryta 18
            #szerokosc meteoryta 30

            # modulowe sprawdzanie kolizji na wszystkich maskach
            if self.spr_kolizje(self.orzel_x+16,self.orzel_y+16,6,13,self.met_tablica[id][0],self.met_tablica[id][1],30,18)==False:
                #jezeli nastapi kolizja
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+22,self.orzel_y+3,8,35,self.met_tablica[id][0],self.met_tablica[id][1],30,18)==False:
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+31,self.orzel_y+13,61,18,self.met_tablica[id][0],self.met_tablica[id][1],30,18)==False:
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+87,self.orzel_y+8,27,5,self.met_tablica[id][0],self.met_tablica[id][1],30,18)==False:
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+92,self.orzel_y+19,7,11,self.met_tablica[id][0],self.met_tablica[id][1],30,18)==False:
                self.kolizja = self.kolizja+1;
        #odejmowanie zycia jezeli za dlugo trwa kolizja
        if self.kolizja>100:
            self.orzel_life=self.orzel_life-1
            self.orzel_zdechl_start = time()
            self.orzel_zdechl_end = time()+3
            self.orzel_zdechl_napis_start = time()
            self.orzel_zdechl_napis_end = time()+1
            self.orzel_zdechl_inicjacja = 1
            #ustawianie orzela na pozycji startowej
            self.orzel_x = 0
            self.orzel_y = 180
            #zerowanie kolizji
            self.kolizja=0

        #jezeli nie ma zycia - game over
        if self.orzel_life==0:
            self.gameover_ground()


    def gui_meteory(self):
        # funkcja odpowiadajaca za aktualizacje i wyswietlanie dolnego gui
        # wyswietlanie zyc

        if self.orzel_life==3:
            #wyswietlanie 3 statku "zycia" na pasku dolnym
            self.glowne.blit(self.orzel_gui, (109,405))
        if self.orzel_life==2 or self.orzel_life==3:
            self.glowne.blit(self.orzel_gui, (56,405))
        if self.orzel_life==2 or self.orzel_life==3 or self.orzel_life==1:
            self.glowne.blit(self.orzel_gui, (3,405))

        if self.mode==1:
            #jezeli meteory
            #wyswietlanie paska postepu lvl
            self.glowne.blit(self.pasek_gui, (162,412))

            #umieszczanie orzela na pasku postepu
            gdzie_orzel = 162+(191*(time()-self.lvl_start))/90
            #print gdzie_orzel
            self.glowne.blit(self.orzel_gui, (gdzie_orzel,405))

        if self.mode==2:
            #jezeli walka z kosmita - render ile ma zycia
            napis = "Kosmita: %s / %d" % (self.kosmita_life_now,self.kosmita_life)
            napis_kosmita = self.menu_font.render(napis,True,(255,255,255))
            self.glowne.blit(napis_kosmita, (170,405))



    def pauza(self):
        #czas_miniony = time()-self.lvl_start
        self.time_pauza = time()
        co = 0
        #ustawienie ekranu pauzy
        self.glowne.blit(self.tlo_pauza,(0,0))
        #render
        pygame.display.flip()
        #wyswietlanie okna pauzy
        while co==0:
            #petla pauzujaca gre
            for event in pygame.event.get():
                if event.type == KEYUP:
                    if (event.key == K_PAUSE):
                        co = 1
                    if (event.key == K_ESCAPE):
                        self.game_exit()

        # aktualizacja czasu
        czas_miniony = time()-self.time_pauza
        self.time_pauza = 0
        self.lvl_start = self.lvl_start+czas_miniony
        self.lvl_end = self.lvl_end+czas_miniony

        if self.orzel_zdechl_inicjacja==1:
            self.orzel_zdechl_end = self.orzel_zdechl_end+czas_miniony
            self.orzel_zdechl_start = self.orzel_zdechl_start+czas_miniony
            self.orzel_zdechl_napis_start = self.orzel_zdechl_napis_start+czas_miniony
            self.orzel_zdechl_napis_end = self.orzel_zdechl_napis_end+czas_miniony

        #usuniecie planszy z pauza - wyczyszczenie ekranu
        self.glowne.fill((0,0,0))

################################################################################

    def lvl_meteoryty(self):

        for event in pygame.event.get():
           # obsluga klawiszy
               if event.type == KEYUP:
                   #print event
                   if (event.key == K_PAUSE):
                       self.pauza()


        #implementacja biblioteki do obslugi klawiszy
        keys = pygame.key.get_pressed()
        # podpisywanie rol danym klawisza
        if keys[K_w]:
            #orzel leci do gory
            self.rusz_orzela(-1)
        if keys[K_s]:
            #orzel leci w dol
            self.rusz_orzela(1)

        #jezeli pierwsze uruchomienie - ustawiamy czas gry
        if self.lvl_start==0:
            self.lvl_start=time();
            self.lvl_end=self.lvl_start+90;

        #jezeli koniec lvl
        if self.lvl_end<=time():
            self.gamestate=0

        #czyscimy ekran - przed renderem
        self.glowne.fill((0,0,0))
        #wyswietlanie tla
        self.glowne.blit(self.tlo, (0,0))

        #wyswietlanie dolnego gui
        self.gui_meteory()

        #ladowanie orzela
        self.load_orzel()
        #orzel leci- wyswietlanie najnowszej pozycji
        #self.glowne.blit(self.orzel,(self.orzel_x,self.orzel_y))

        if self.orzel_zdechl_inicjacja==1:
            #chwila odpoczynku po kolizji z meteorytem
            if self.orzel_zdechl_end>time():
                if self.orzel_zdechl_napis_end>=time():
                    #wyswietlanie napisu BOOM!
                    boom = self.menu_font.render("BOOM!",True,(255,255,255))
                    self.glowne.blit(boom, (200, 200))
                    #print "boo1"
            if self.orzel_zdechl_end<time():
                #konczymy ochronke
                #jezeli juz czas ochronny sie skonczyl
                self.orzel_zdechl_inicjacja = 0
                self.orzel_zdechl_end = 0
                self.orzel_zdechl_start = 0
                self.orzel_zdechl_napis_end = 0
                self.orzel_zdechl_napis_start = 0
                #czyszczenie tablicy z meteorami
                self.met_tablica = []
                #print "boo2"
        #normalne wyswietlanie meteorytow i kolizji
        else:
            if self.godmode==0:
                #sprawdzanie, czy nie wystapila kolizja miedzy orzelem, a meteorytami
                self.kolizje()
            #usuwanie meteorytow z listy ktore wyszly za plansze
            self.del_met()
            #ladowanie meteorytow
            self.generate_met()

        # przenosimy buffor na ekran - rendering
        pygame.display.flip()

        if self.lvl_end<=time():
            self.pomiedzy_ground()

################################################################################

    def start_menu(self):
        # ekran menu startowego
        self.lvl_end=0
        self.lvl_start=0
        self.orzel_life=3
        dziejesie=0
        co=1

        start = self.menu_font.render("GRAJ!",True,(255,255,255))
        koniec = self.menu_font.render("KONIEC",True,(255,255,255))
        while dziejesie==0:
            #czyszczenie ekranu
            self.glowne.fill((0,0,0))
            #inicjowanie tla
            self.glowne.blit(self.tlo_menu, (0,0))

            if co==1:
                #wybrany jest start gry
                self.glowne.blit(self.menu_belka, (50,150))
            else:
                #wybrany jest exit
                self.glowne.blit(self.menu_belka, (50,200))

            #wyswietlanie napisow na belce
            self.glowne.blit(start, (100, 160))
            self.glowne.blit(koniec, (100, 210))

            pygame.display.flip()

            #print "dziala"

            for event in pygame.event.get():
            # obsluga klawiszy
                if event.type == KEYUP:
                    #print event
                    if event.key == K_w or event.key == K_s:
                        #wybor pozycji
                        if co==1:
                            co=2
                        else:
                            co=1
                    if event.key == K_SPACE or event.key == K_RETURN:
                        if co==1:
                            dziejesie=1
                            self.mode=1
                        if co==2:
                            dziejesie=1
                            self.gamestate=0

################################################################################

    def gameover_ground(self):
        #wyswietlana plansza kiedy skoncza sie zycia orzelowi

        gdziejak = 0

        while gdziejak==0:
            #czyscimy ekran
            self.glowne.fill((0,0,0))
            self.glowne.blit(self.tlo_gameover,(0,0))

            pygame.display.flip()

            for event in pygame.event.get():
                if event.type == KEYUP:
                    #powrot do menu glownego
                    if event.key == K_SPACE or event.key == K_RETURN:
                        self.mode=0
                        gdziejak=1
                    #wyjscie z gry
                    if event.key == K_ESCAPE:
                        self.game_exit()

################################################################################

    def pomiedzy_ground(self):
        #pojawia sie po ukonczeniu lvl z meteorytami, a przed lvl z kosmita

        time_ile = time()+2

        #czyscimy ekran
        self.glowne.fill((0,0,0))
        self.glowne.blit(self.tlo_misja,(0,0))

        #tworzenie napisow
        misja = self.menu_font.render("Misja: zgladz Kurvinox'a",True,(255,255,255))
        miejsce = self.menu_font.render("Miejsce: glebia kosmosu",True,(255,255,255))
        self.glowne.blit(misja,(50,200))
        self.glowne.blit(miejsce,(50,230))

        pygame.display.flip()

        #czekanie 2 sekundy
        while time_ile>=time():
            co=1

        #wyswietlanie
        self.mode=2

################################################################################

    def lvl_kosmita(self):

        #zerowanie konfiga
        self.orzel_x=0
        self.orzel_y=180
        self.orzel_life=3
        self.kolizja=0
        self.met_tablica = []
        self.petla=1



        while self.petla==1:
            for event in pygame.event.get():
            # obsluga klawiszy
                if event.type == KEYUP:
                    #print event
                    if (event.key == K_PAUSE):
                        self.pauza()
                    if event.key == K_ESCAPE:
                        self.game_exit()

            #implementacja biblioteki do obslugi klawiszy
            keys = pygame.key.get_pressed()
            # podpisywanie rol danym klawisza
            if keys[K_w]:
                #orzel leci do gory
                self.rusz_orzela(-1)
            if keys[K_s]:
                #orzel leci w dol
                self.rusz_orzela(1)
                #lvl - walka z kosmita
            if keys[K_SPACE]:
                #orzel strzela
                self.fire_orzel()

            #czyscimy ekran - przed renderem
            self.glowne.fill((0,0,0))
            #wyswietlanie tla
            self.glowne.blit(self.tlo, (0,0))

            #load gui
            self.gui_meteory()

            #wyswietlanie pociskow
            self.load_pociski()

            #wyswietlanie orzela
            self.load_orzel()

            #wyswietlanie kosmity
            self.load_kosmita()

            #poruszanie pociskow
            self.rusz_pociski()

            #sprawdzanie kolizji
            self.kolizja_pociski()

            #renderowanie obrazu
            pygame.display.flip()

        #po walce wygranej z kosmita
        self.mode=3

################################################################################

    def load_kosmita(self):
        #ladowanie kosmity oraz jego AI
        #wyswietlanie kosmity na jego aktualnej pozycji
        self.glowne.blit(self.kosmita[self.kosmita_ktory],(self.kosmita_x,self.kosmita_y))

        #tablica decyzyjna dla kosmity
        #1 = w gore
        #2 = w dol
        #3 = zostac w miejscu
        temp_tablica = []

        #AI
        for i in range(len(self.pociski_orzel)):
            #analizowanie ruchow na bazie nadlatujacych pociskow od orzela
            if (self.pociski_orzel[i][1]<self.kosmita_y) and (self.pociski_orzel[i][1]>self.kosmita_y+40):
                #jezeli pocisk jest w stanie trafic kosmite na tej pozycji gdzie jest
                if self.orzel_y<self.kosmita_y:
                    #orzel jest nizej - przemieszczamy sie na dol
                    temp_tablica.append(2)
                else:
                    #orzel jest wyzej - lecimy na gore
                    temp_tablica.append(1)

            elif (self.pociski_orzel[i][1]<self.kosmita_y):
                #pocisk jest nad kosmita - w dol
                temp_tablica.append(2)

            elif (self.pociski_orzel[i][1]>self.kosmita_y+40):
                #pocisk jest pod kosmita - w gore
                temp_tablica.append(1)



            #analizowanie gdzie jest orzel
            if self.orzel_y<self.kosmita_y:
                #jezeli orzel jest wyzej - do gory
                temp_tablica.append(1)


            if self.orzel_y>self.kosmita_y:
                #jezeli orzel jest nizej - w dol
                temp_tablica.append(2)



        #jezeli orzel jest w poblizu karabinka kosmity - strzelamy
        if (self.orzel_y<self.kosmita_y+20) and (self.orzel_y+40>self.kosmita_y+20):
            #kosmita strzela
            self.fire_kosmita()
            if temp_tablica.count(1)>temp_tablica.count(2):
                temp_tablica.append(1)
            else:
                temp_tablica.append(2)


        #czynnik losowy - glupoty kosmitow
        for i in range(0,3):
            temp_tablica.append(randint(1, 2))

        if self.orzel_y>self.kosmita_y or self.orzel_y<self.kosmita_y:
            if temp_tablica.count(1)>temp_tablica.count(2):
                temp_tablica.append(1)
            else:
                temp_tablica.append(2)

        if temp_tablica.count(1)>temp_tablica.count(2):
            temp_tablica.append(1)
            temp_tablica.append(1)
            temp_tablica.append(1)
            temp_tablica.append(1)
            temp_tablica.append(1)
        else:
            temp_tablica.append(2)
            temp_tablica.append(2)
            temp_tablica.append(2)
            temp_tablica.append(2)
            temp_tablica.append(2)




        #analiza checi ruchow kosmity
        if temp_tablica.count(1)>temp_tablica.count(2) and temp_tablica.count(1)>temp_tablica.count(3):
            #jezeli najwiecej jest w gore
            self.kosmita_y = self.kosmita_y-(1*self.kosmita_speed)
        elif temp_tablica.count(2)>temp_tablica.count(1) and temp_tablica.count(2)>temp_tablica.count(1):
            #jezeli najwiecej jest w dol
            self.kosmita_y = self.kosmita_y+(1*self.kosmita_speed)


        #czyszczenie pamieci z tablicy decyzyjnej kosmity
        del(temp_tablica)

        #jezeli kosmita jest poza plansza - przeniesie na gore
        if self.kosmita_y > 360:
            self.kosmita_y=0
        if self.kosmita_y < 0:
            self.kosmita_y=360






################################################################################

    def fire_orzel(self):
        #jezeli nastapi nacisniecie spacji przez gracza - dorzucenie pocisku do tablicy
        if self.pocisk_time_ostatni<=time():
            #jezeli ostatni pocisk zostal wystrzelony jakis czas temu
            #dodawanie go do tablicy
            self.pociski_orzel.append([self.orzel_x+95,self.orzel_y+25])
            #tworzenie nowego ograniczenia czasowego
            self.pocisk_time_ostatni=time()+self.pociski_time

################################################################################

    def load_pociski(self):
        #ladowanie i wyswietlanie pociskow
        #pociski od orzela
        for i in range(len(self.pociski_orzel)):
            self.glowne.blit(self.pocisk_orzel,(self.pociski_orzel[i][0],self.pociski_orzel[i][1]))

        #pociski kosmity
        for i in range(len(self.pociski_kosmita)):
            self.glowne.blit(self.pocisk_kosmita,(self.pociski_kosmita[i][0],self.pociski_kosmita[i][1]))


################################################################################

    def rusz_pociski(self):
        #przemieszczanie pociskow orzela i kosmity
        # self.pociski_orzel[i][0] - pozycja x pocisku
        # self.pociski_orzel[i][1] - pozycja y pocisku
        #pociski orzela
        for i in range(len(self.pociski_orzel)):
            if self.pociski_orzel[i][0]==403:
                #jezeli pocisk jest juz za plansza - usuniecie go z tablicy
                del(self.pociski_orzel[i])
            else:
                #poruszanie pociska
                self.pociski_orzel[i][0] = self.pociski_orzel[i][0]+self.pociski_szybkosc

        #pociski kosmity - leca w druga strone
        for i in range(len(self.pociski_kosmita)):
            if self.pociski_kosmita[i][0]==-3:
                #jezeli pocisk jest juz za plansza - usuniecie go z tablicy
                del(self.pociski_kosmita[i])
            else:
                #poruszanie pociska
                self.pociski_kosmita[i][0] = self.pociski_kosmita[i][0]-self.pociski_szybkosc

################################################################################

    def fire_kosmita(self):
        #strzelanie kosmitow
        if self.kosmita_pociski_time_ostatni<=time():
            #jezeli ostatni pocisk zostal wystrzelony jakis czas temu
            #dodawanie do tablicy
            self.pociski_kosmita.append([self.kosmita_x,self.kosmita_y+20])
            self.kosmita_pociski_time_ostatni=time()+self.kosmita_pociski_time

################################################################################

    def kolizja_pociski(self):
        #sprawdzanie czy nastapila kolizja pociskow orzela miedzy kosmita
        #oraz kosmity miedzy orzelem



        #tablica tymczasowa
        tablica_temp = []

        #print tablica_temp

        for id in range(len(self.pociski_kosmita)):
            #self.met_tablica[id][0] - pozycja x meteoryta
            #self.met_tablica[id][1] - pozycja y meteoryta
            #wysokosc pocisku 2
            #szerokosc pocisku 3

            # modulowe sprawdzanie kolizji na wszystkich maskach
            if self.spr_kolizje(self.orzel_x+16,self.orzel_y+16,6,13,self.pociski_kosmita[id][0],self.pociski_kosmita[id][1],3,2)==False:
                #jezeli nastapi kolizja
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+22,self.orzel_y+3,8,35,self.pociski_kosmita[id][0],self.pociski_kosmita[id][1],3,2)==False:
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+31,self.orzel_y+13,61,18,self.pociski_kosmita[id][0],self.pociski_kosmita[id][1],3,2)==False:
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+87,self.orzel_y+8,27,5,self.pociski_kosmita[id][0],self.pociski_kosmita[id][1],3,2)==False:
                self.kolizja = self.kolizja+1;
            if self.spr_kolizje(self.orzel_x+92,self.orzel_y+19,7,11,self.pociski_kosmita[id][0],self.pociski_kosmita[id][1],3,2)==False:
                self.kolizja = self.kolizja+1;

            if self.kolizja>0:
                #umieszczanie do tablicy pociskow ktore trafily
                #print id
                tablica_temp.append(id)

            self.kolizja=0




        #print tablica_temp
            #usuniecie z tablicy - trafiony!
        if self.godmode==0:
            ile = len(tablica_temp)
            for id in range(ile):
                #odejmowanie zycia od orzela o taka ilosc ile trafien bylo
                self.orzel_life = self.orzel_life-ile
                #print tablica_temp[id]
                #print self.pociski_kosmita[tablica_temp[id]]
                del(self.pociski_kosmita[tablica_temp[id]])

        if self.orzel_life<=0:
                #GAME OVER
                self.gameover_ground()



            #czyszczenie tablicy tymczasowej
        tablica_temp = []
            # sprawdzanie kolizji dla kosmity
        for id in range(len(self.pociski_orzel)):
            #self.met_tablica[id][0] - pozycja x meteoryta
            #self.met_tablica[id][1] - pozycja y meteoryta
            #wysokosc pocisku 2
            #szerokosc pocisku 3

            # modulowe sprawdzanie kolizji na wszystkich maskach
            if self.spr_kolizje(self.kosmita_x,self.kosmita_y,29,40,self.pociski_orzel[id][0],self.pociski_orzel[id][1],3,2)==False:
                    #jezeli nastapi kolizja
                self.kolizja = self.kolizja+1;

            if self.kolizja>0:
                #umieszczanie do tablicy pociskow ktore trafily
                tablica_temp.append(id)
                #print self.kosmita_life_now

            self.kolizja=0



        #print tablica_temp
            #usuniecie z tablicy - trafiony!
        ile = len(tablica_temp)
        for id in range(len(tablica_temp)):
            self.kosmita_life_now = self.kosmita_life_now-ile
            del(self.pociski_orzel[tablica_temp[id]])

        if self.kosmita_life_now<=0:
            #GAME OVER
                self.petla=0

        tablica_temp = []









################################################################################







































############################################################## glowna petla

    def loop(self):

        while self.gamestate==1:

            if self.mode==0:
                #ekran startowy
                self.start_menu()
            elif self.mode==1:
                #gra - meteoryty
                self.lvl_meteoryty()
            elif self.mode==2:
                #gra - walka z kosmita
                self.lvl_kosmita()
            elif self.mode==3:
                self.game_exit()






        #wywolanie zakonczenia programu jezeli gamestate==0
        #wp = raw_input("KURWA")
        self.game_exit()

if __name__ == '__main__':
   Patrol();