#includowanie biblioteki pygame
import pygame, sys,os
from pygame.locals import *

#include obslugi klawiszy
#from pygames.locals import *
# inicjowanie okna
pygame.init()

# tworzenie okna
okno = pygame.display.set_mode((400,400), 0, 32)

# ustawienie tytulu okna
pygame.display.set_caption("Gwiezdny Patrol")

# wczytytanie tla
tlo = pygame.image.load("img/kosmos.jpg").convert()
# umieszczanie obrazka tla na ekranie
okno.blit(tlo, (0,0))

# wczytywanie orzela
orzel = pygame.image.load("img/orzel_mini.png").convert()
okno.blit(orzel, (50,50))

# update ekranu
pygame.display.update()

while True:
    pressed_keys = pygame.key.get_pressed()
    if pressed_keys[K_SPACE]:
        okno.blit(orzel, (100,100))


