#/usr/bin/env python

import pygame
from pygame.locals import *

import random

from PixelPerfect import *

def makeFoo(x,y):
    img = pygame.Surface([20,20])
    img = img.convert()
    img.fill((0xff, 0xff, 0xff))
    img.set_colorkey((0xff, 0xff, 0xff), RLEACCEL)
    pygame.draw.line(img, (255,0,0), (0,0), (19,19), 3)
    pygame.draw.line(img, (255,0,0), (0,19), (19,0), 3)
    foo = pygame.sprite.Sprite()
    foo.image = img
    foo.rect = img.get_rect()
    foo.rect.centerx = x
    foo.rect.centery = y
    # set the hitmask value
    foo.hitmask = pygame.surfarray.array_colorkey(img)
    return foo

def main():
    # init pygame
    pygame.init()    

    # setup the display
    screen = pygame.display.set_mode((600, 600),1)
    pygame.display.set_caption('Toggle detection method with space.')        

    # make a boom text surface
    font = pygame.font.Font(None, 72)
    boom_text = font.render("BOOOOM!", 1, (0, 0, 0))
    boom_textpos = boom_text.get_rect()
    boom_textpos.centerx = 300
    boom_textpos.centery = 300

    # create a very simple hero sprite
    img = pygame.Surface([40,40])
    img = img.convert()
    img.fill((0xff, 0xff, 0xff))
    img.set_colorkey((0xff, 0xff, 0xff), RLEACCEL)
    pygame.draw.line(img, (0,0,255), (20,0), (20,40), 3)
    pygame.draw.line(img, (0,0,255), (0,20), (40,20), 3)
    hero = pygame.sprite.Sprite()
    hero.image = img
    hero.rect = img.get_rect()
    hero.rect.centerx = 150
    hero.rect.centery = 150
    # you can use either array_colorkey() or array_alpha() as hitmask
    hero.hitmask = pygame.surfarray.array_colorkey(img)
    hero_group = pygame.sprite.RenderPlain(hero)
    
    # create some foo sprites
    foo_group = pygame.sprite.RenderPlain()
    for i in range(200):
        foo_group.add(makeFoo(random.randint(0,600),random.randint(0,600)))

    test_types=["Rectangle","PixelPerfect"]

    clock = pygame.time.Clock()
    dx = 0
    dy = 0
    testPixels = 1

    font = pygame.font.Font(None, 18)
    while 1:
        clock.tick(30)        
    
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return
                elif event.key == K_UP:
                    dy = -1
                elif event.key == K_DOWN:
                    dy = 1
                elif event.key == K_LEFT:
                    dx = -1
                elif event.key == K_RIGHT:
                    dx = 1
                elif event.key == K_SPACE:
                    if testPixels :
                        testPixels = 0
                    else:
                        testPixels = 1
            elif event.type == KEYUP:                
                if event.key == K_ESCAPE:
                    return
                elif event.key == K_UP and dy == -1:
                    dy = 0
                elif event.key == K_DOWN and dy == 1:
                    dy = 0
                elif event.key == K_LEFT and dx == -1:
                    dx = 0
                elif event.key == K_RIGHT and dx == 1:
                    dx = 0
            elif event.type == MOUSEBUTTONDOWN:
                hero.rect.centerx = event.pos[0]   
                hero.rect.centery = event.pos[1]   
            elif event.type == MOUSEMOTION:
                if event.buttons[0]:
                    hero.rect.centerx = event.pos[0]   
                    hero.rect.centery = event.pos[1]          

        # update the hero location
        hero.rect.x += dx
        hero.rect.y += dy        

        # clear screen
        screen.fill((255,255,255))           
        
        # check for collisions

        ticks = pygame.time.get_ticks()        
        if testPixels:
            li = spritecollide_pp(hero, foo_group,0)
        else:
            li = pygame.sprite.spritecollide(hero, foo_group, 0)
        time = pygame.time.get_ticks()-ticks
                                   
        # update the screen                                
        foo_group.draw(screen)
        hero_group.draw(screen)            

        if len(li):
            screen.blit(boom_text, boom_textpos)                    

        time_text = font.render("Method: %s Ms:%d" % (test_types[testPixels],time), 1,(0,0,0))
        screen.blit(time_text,(0,0))        pygame.display.flip()
             
 
#this calls the 'main' function when this script is executed
if __name__ == '__main__': main()
