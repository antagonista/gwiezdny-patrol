#include bibliotek
import pygame, sys,os
from pygame.locals import *

class Patrol(object):
    def __init__(self):
        # ruszamy z kopyta -> pygame
        pygame.init()

        # konfig ekranu
        self.glowne = pygame.display.set_mode((400,400),DOUBLEBUF)
        # ustawienie tytulu okna
        pygame.display.set_caption("Gwiezdny Patrol")

        # ogolny konfig
        # stan gry: 1 - start; 0 - wyjscie
        self.gamestate = 1
        self.orzel_szybkosc = 0.5 # szybkosc orzela
        self.orzel_x = 0   # pozycja X orzela
        self.orzel_y = 180   # startowa pozycja Y orzela
        self.orzel = pygame.image.load("img/orzel_mini.png").convert() # wczytanie obrazka orzela

        # wczytytanie tla
        self.tlo = pygame.image.load("img/kosmos.jpg").convert()
        # umieszczanie obrazka na ekranie


        # glowna petla
        self.loop()
    # konczenie programu
    def game_exit(self):
        # koniec pracy na dzisiaj
        exit()

    def rusz_orzela(self,y):
        #modyfikacja pozycji orzela
        self.orzel_y = self.orzel_y + (y * self.orzel_szybkosc)
        if self.orzel_y>400:
            self.orzel_y = 0;
        if self.orzel_y<0:
            self.orzel_y = 400;

    def load_orzel(self):
        #ladowanie orzela + animacja



############################################################## glowna petla

    def loop(self):

        while self.gamestate==1:
           for event in pygame.event.get():
           # obsluga klawiszy
               if event.type==QUIT:
                   self.gamestate=0

           #implementacja biblioteki do obslugi klawiszy
           keys = pygame.key.get_pressed()
           # podpisywanie rol danym klawisza
           if keys[K_w]:
                #orzel leci do gory
                self.rusz_orzela(-1)
           if keys[K_s]:
                #orzel leci w dol
                self.rusz_orzela(1)


           #czyscimy ekran - przed renderem
           self.glowne.fill((0,0,0))
           #wyswietlanie tla
           self.glowne.blit(self.tlo, (0,0))

           #orzel leci- wyswietlanie najnowszej pozycji
           self.glowne.blit(self.orzel,(self.orzel_x,self.orzel_y))

           # przenosimy buffor na ekran - rendering
           pygame.display.flip()

        #wywolanie zakonczenia programu jezeli gamestate==0
        self.game_exit()

if __name__ == '__main__':
   Patrol();