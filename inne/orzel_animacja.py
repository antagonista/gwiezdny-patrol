#include bibliotek
import pygame, sys,os
from random import *
from time import *
from pygame.locals import *

class Patrol(object):
    def __init__(self):
        # ruszamy z kopyta -> pygame
        pygame.init()

        # konfig ekranu
        self.glowne = pygame.display.set_mode((400,400),DOUBLEBUF)
        # ustawienie tytulu okna
        pygame.display.set_caption("Gwiezdny Patrol")

        # ogolny konfig
        # stan gry: 1 - start; 0 - wyjscie
        self.gamestate = 1
        self.orzel_szybkosc = 0.5 # szybkosc orzela
        self.orzel_x = 0   # pozycja X orzela
        self.orzel_y = 180   # startowa pozycja Y orzela
        self.orzel_animacja = 1;
        #ustalanie wartosci modyfikowalnych dla meteorow
        self.met_max = 10; #max meteorow na planszy
        self.met_szybkosc = 0.1 #szybkosc poruszania sie meteorow
        self.met_time_ile = 1
        self.met_time = 0 # czas kiedy ostatni meteoryt wygenerowano
        self.met_tablica = [] #tablica z wspolrzednymi meteorytow

        # wczytytanie tla
        self.tlo = pygame.image.load("img/kosmos.jpg").convert()
        # wczytanie obrazka meteorytu
        self.met = pygame.image.load("img/met_1.png").convert_alpha()


        # glowna petla
        self.loop()
    # konczenie programu
    def game_exit(self):
        # koniec pracy na dzisiaj
        exit()

    def rusz_orzela(self,y):
        #modyfikacja pozycji orzela
        self.orzel_y = self.orzel_y + (y * self.orzel_szybkosc)
        if self.orzel_y>360:
            self.orzel_y = 0;
        if self.orzel_y<0:
            self.orzel_y = 360;

    def load_orzel(self):

        #ladowanie orzela + animacja
        url = "img/orzel/orzel_%s.png" % (self.orzel_animacja)
        # wczytanie obrazka orzela + ustalanie przezroczystoscia dla PNG
        self.orzel = pygame.image.load(url).convert_alpha()

        #animacja orzela
        if self.orzel_animacja==6:
            self.orzel_animacja=1;
        else:
            self.orzel_animacja = self.orzel_animacja+1
        # END animacja orzela

        return self.orzel

    def generate_met(self):
        #generowanie meteorytow
        if (time()>self.met_time) and (len(self.met_tablica)<self.met_max):
            #jezeli czas od utworzenia ostatniego meteorytu jest mniejszy
            # i jezeli w tablicy jeszcze nie ma max mozliwych meteorow
            # GENERUJEMY METEORYT

            #losowa pozycja nowego meteorytu na Y
            met_y = randint(0, 382)
            self.met_tablica.append([400,met_y])
            self.met_time=time()+self.met_time_ile

        for id in range(len(self.met_tablica)):
            # przesowanie meteorytow
            # umieszczanie meteorytow na planszy
            # enetualnie usuwanie kiedy sa po za plansza

            # poruszanie meteoryta
            zmiana = self.met_tablica[id][0];
            zmiana = zmiana + (-1 * self.met_szybkosc)
            self.met_tablica[id][0] = zmiana
            del(zmiana)
            # dodawanie go do renderowania
            self.glowne.blit(self.met,(self.met_tablica[id][0],self.met_tablica[id][1]))

    def del_met(self):
        #tworzenie tymczasowej tablicy z rekordami, ktore maja byc usuniete
        temp_tablica = []
        for id in range(len(self.met_tablica)):
            if -30>=self.met_tablica[id][0]:
                temp_tablica.append(id)

        for id in range(len(temp_tablica)):
            del(self.met_tablica[temp_tablica[id]])

    def spr_kolizje(self,x1,y1,w1,h1,x2,y2,w2,h2):
        if x1 >= x2+w2: return True
        if x1+w1 <= x2: return True
        if y1 >= y2+h2: return True
        if y1+h1 <= y2: return True
        return False

    def kolizje(self):
        #zmienna kolizja zmienia wartosc kiedy nastapi kolizja
        kolizja = 0
        #self.orzel_x - pozycja x orzela
        #self.orzel_y - pozucja y orzela
        #wysokosc orzela 40
        #szerokosc orzela 100

        for id in range(len(self.met_tablica)):
            #self.met_tablica[id][0] - pozycja x meteoryta
            #self.met_tablica[id][1] - pozycja y meteoryta
            #wysokosc meteoryta 18
            #szerokosc meteoryta 30

            if self.spr_kolizje(self.orzel_x,self.orzel_y,100,40,self.met_tablica[id][0],self.met_tablica[id][1],18,30)==False:
                #jezeli nastapi kolizja
                kolizja = kolizja+1;
                return "kolizja"












############################################################## glowna petla

    def loop(self):

        while self.gamestate==1:
           for event in pygame.event.get():
           # obsluga klawiszy
               if event.type==QUIT:
                   self.gamestate=0

           #implementacja biblioteki do obslugi klawiszy
           keys = pygame.key.get_pressed()
           # podpisywanie rol danym klawisza
           if keys[K_w]:
                #orzel leci do gory
                self.rusz_orzela(-1)
           if keys[K_s]:
                #orzel leci w dol
                self.rusz_orzela(1)


           #czyscimy ekran - przed renderem
           self.glowne.fill((0,0,0))
           #wyswietlanie tla
           self.glowne.blit(self.tlo, (0,0))

           #ladowanie orzela
           self.orzel=self.load_orzel()
           #orzel leci- wyswietlanie najnowszej pozycji
           self.glowne.blit(self.orzel,(self.orzel_x,self.orzel_y))

           #sprawdzanie, czy nie wystapila kolizja miedzy orzelem, a meteorytami
           print self.kolizje()


           #usuwanie meteorytow z listy ktore wyszly za plansze
           self.del_met()
           #ladowanie meteorytow
           self.generate_met()

           # przenosimy buffor na ekran - rendering
           pygame.display.flip()

        #wywolanie zakonczenia programu jezeli gamestate==0
        self.game_exit()

if __name__ == '__main__':
   Patrol();